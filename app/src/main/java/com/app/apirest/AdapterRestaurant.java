package com.app.apirest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AdapterRestaurant extends RecyclerView.Adapter<AdapterRestaurant.MyViewHolder> {

    private ArrayList<Restaurant> myDataSet;
    private OnClickItem myOnClickItem;


    //Constructor
    public AdapterRestaurant(ArrayList<Restaurant> myDataSet, OnClickItem myOnClickItem) {
        this.myDataSet = myDataSet;
        this.myOnClickItem = myOnClickItem;
    }

    //ViewHolder
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnClickItem myOnClickItem;

        //Variables
        CardView myCard;
        TextView myText;

        public MyViewHolder(View itemView, OnClickItem onClickItem) {

            super(itemView);

            myText = itemView.findViewById(R.id.myText);
            myCard = itemView.findViewById(R.id.myCard);
            myCard.setOnClickListener(this);
            myOnClickItem = onClickItem;
        }

        @Override
        public void onClick(View v) {
            myOnClickItem.onClickPosition(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public AdapterRestaurant.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,null,false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new AdapterRestaurant.MyViewHolder(view,myOnClickItem);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRestaurant.MyViewHolder holder, final int position) {
        holder.myText.setText(myDataSet.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }

    public interface OnClickItem{
        void onClickPosition(int position);
    }

}
