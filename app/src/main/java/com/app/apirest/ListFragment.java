package com.app.apirest;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListFragment extends Fragment implements AdapterRestaurant.OnClickItem {


    private RecyclerView myRecycler;
    private AdapterRestaurant adapterFood;
    private ArrayList<Restaurant> listRestaurants;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        listRestaurants = new ArrayList<>();

        myRecycler = view.findViewById(R.id.myRecycler);
        myRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        getRestaurant_cloud();

        return view;
    }

    @Override
    public void onClickPosition(int position) {
        Toast.makeText(getContext(), "Seleccionaste el Card: "+position+" - "+listRestaurants.get(position).getName(), Toast.LENGTH_SHORT).show();
    }

    public void getRestaurant_cloud(){

        final Retrofit retrofit = new Retrofit.Builder().baseUrl("https://app-delivery-280704.uc.r.appspot.com/api/").addConverterFactory(GsonConverterFactory.create()).build();
        Api api = retrofit.create(Api.class);
        Call<ArrayList<Restaurant>> call = api.getRestaurant();
        call.enqueue(new Callback<ArrayList<Restaurant>>() {
            @Override
            public void onResponse(Call<ArrayList<Restaurant>> call, Response<ArrayList<Restaurant>> response) {
                if(response.isSuccessful()){

                    ArrayList<Restaurant> restaurantArrayList = response.body();

                    for(int i=0 ; i < restaurantArrayList.size(); i++){
                        listRestaurants.add(new Restaurant(restaurantArrayList.get(i).getName()));
                    }

                    printCards();
                }
                else{
                    Toast.makeText(getContext(), "Conexión débil", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Restaurant>> call, Throwable t) {
                Toast.makeText(getContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void printCards(){
        adapterFood = new AdapterRestaurant(listRestaurants,this);
        myRecycler.setAdapter(adapterFood);
    }
}