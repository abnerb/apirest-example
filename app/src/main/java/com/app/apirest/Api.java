package com.app.apirest;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("restaurant")
    Call<ArrayList<Restaurant>> getRestaurant();
}
