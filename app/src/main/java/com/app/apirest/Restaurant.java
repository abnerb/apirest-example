package com.app.apirest;

import com.google.gson.annotations.SerializedName;

public class Restaurant {

    @SerializedName("name")
    private String name;

    public Restaurant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
